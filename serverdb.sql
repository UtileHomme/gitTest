Jatin's updates
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 10:30 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `abdomens`
--

CREATE TABLE `abdomens` (
  `id` int(11) NOT NULL,
  `Inspection` varchar(45) DEFAULT NULL,
  `AusculationofBS` varchar(45) DEFAULT NULL,
  `Palpation` varchar(45) DEFAULT NULL,
  `Percussion` varchar(45) DEFAULT NULL,
  `Ileostomy` varchar(45) DEFAULT NULL,
  `Colostomy` varchar(45) DEFAULT NULL,
  `Functioning` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `District` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `PinCode` varchar(8) DEFAULT NULL,
  `PAddress1` varchar(100) DEFAULT NULL,
  `PAddress2` varchar(100) DEFAULT NULL,
  `PCity` varchar(45) DEFAULT NULL,
  `PDistrict` varchar(100) DEFAULT NULL,
  `PState` varchar(45) DEFAULT NULL,
  `PPinCode` varchar(8) DEFAULT NULL,
  `EAddress1` varchar(100) DEFAULT NULL,
  `EAddress2` varchar(100) DEFAULT NULL,
  `ECity` varchar(45) DEFAULT NULL,
  `EDistrict` varchar(100) DEFAULT NULL,
  `EState` varchar(45) DEFAULT NULL,
  `EPinCode` varchar(8) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$uc.c5pVBtF0vYIzmBSIn0en6KDWAbc.9Fkf8j5xSHIPOtgFx9yv2q', NULL, NULL, NULL),
(8, 'Mruthyunjaya', 'mruthyunjaya@healthheal.in', '$2y$10$xFhn6EIep2HIotBudIo2auQuqKgft7AKjKe4jfZMkYBW09kY5eR3S', NULL, '2017-05-31 00:59:24', '2017-05-31 00:59:24'),
(3, 'murtaza', 'murtazasalumber007@gmail.com', '$2y$10$rtFzwrtGcb8SHoiVSKuL0.EzTi2fUyUCahRmHQELjmPgeDJpvqz5.', 'VKxf3sIObJUbJBDsCFZXVPaYfnSiqzjsirdBVMpdlDDnFsWxQoKOGhmCCAbm', '2017-05-26 05:59:59', '2017-05-26 02:25:31'),
(7, 'Prabhakar', 'prabhakar@healthheal.in', '$2y$10$aZhDNqld8eXv7oETovbCsuZ35vnkWc2doZXzrtdLnUR2PlE9txiFe', NULL, '2017-05-31 00:58:19', '2017-05-31 00:58:19'),
(6, 'Rohan', 'rohan@healthheal.in', '$2y$10$veiyKN/Ff5CW5YydBfKOAumfr0sF2dzsdLBk61Zb1nQFSmBGwdx5m', NULL, '2017-05-31 00:57:06', '2017-05-31 00:57:06'),
(9, 'Neeraj', 'neeraj.patil@healthheal.in', '$2y$10$K5sHh.tmm69Z2UprcckTweY7bE.uhy8cmfMMLiRZemlJLC3JAOJ5q', NULL, '2017-05-31 01:00:24', '2017-05-31 01:00:24'),
(10, 'Murtaza', 'murtaza.s@healthheal.in', '$2y$10$E.FGitQQvVPiwHk04vRnau9Wdjah9DtnDWFkRHEvYLl3w3u1IwpyC', NULL, '2017-05-31 01:01:40', '2017-05-31 01:01:40'),
(11, 'Sundara', 'sundara.kv@healthheal.in', '$2y$10$GJhTnuAR8hsGTmROQbH.neRRoxXjR/7qTPugjxOfdWDel/C0i3rdG', NULL, '2017-05-31 01:02:45', '2017-05-31 01:02:45'),
(12, 'Ligo', 'ligo.v@healthheal.in', '$2y$10$dgHFgOg7hzcZ5gWrZPZ9I.EOoaplhs/l8hv0Ey/hipfpWEYOBUKzO', NULL, '2017-05-31 01:03:36', '2017-05-31 01:03:36'),
(13, 'Sagar', 'sagar.br@healthheal.in', '$2y$10$I8HGdElvw3hD9x8i8xZImuiviVTbihKstEGwOe3.X5RpOCiRfZVuy', NULL, '2017-05-31 02:53:27', '2017-05-31 02:53:27'),
(14, 'Venkatesha', 'venkatesha.hg@healthheal.in', '$2y$10$E7mXtM8AnmVgCqUDKidoUuKttqifMBTtjSWbtj8yxhaADZaTrTdRW', NULL, '2017-05-31 02:54:45', '2017-05-31 02:54:45'),
(15, 'Yathisha', 'yathisha.m@healthheal.in', '$2y$10$426fDUD0xyK0MPRCU1Qv8OnYqXISAWBNCbHHcCITthr7aAD.QHYCO', NULL, '2017-05-31 02:55:50', '2017-05-31 02:55:50'),
(16, 'Shiju', 'shiju.k@healthheal.in', '$2y$10$cAVrNLPI6GNZXeRSAS40E.Lyw/QO94/J0zgRx4SAcjWnP49xM/4.y', NULL, '2017-05-31 02:56:49', '2017-05-31 02:56:49'),
(17, 'Sowmya', 'sowmya.m@healthheal.in', '$2y$10$5x6s8TaVt/PQE.PztlVsIOZf2n6b.yI79yWpD49PUGif1E2zK7X7e', NULL, '2017-06-01 05:39:43', '2017-06-01 05:39:43'),
(19, 'Padmashree', 'padmashree.n@healthheal.in', '$2y$10$Nrdhob83d5zuclKSCRkPMutvLrHtcJ0DoWIqukhwYAf/Y/3dbdJvG', 'uhGUwgjX6ETYJNMxRlFKpWJf2uWK7ui3cvmVrUUxGfLSb6Z5N6rUMMsyreK6', '2017-06-01 06:30:36', '2017-06-01 06:30:36'),
(20, 'Leelambika', 'leelambika@healthheal.in', '$2y$10$qNKlQYXIHASe8JjeiGnwW.fwhnWrkrGbi3PAU48KUIIe9KownvI5q', NULL, '2017-06-01 23:49:34', '2017-06-01 23:49:34'),
(21, 'Avinash', 'avinash.kv@healthheal.in', '$2y$10$KveVhPQ2bO5LEW84PSXJPeJQRM5yoz693MOqszBZ5l/yx9RZP.LcG', NULL, '2017-06-01 23:50:44', '2017-06-01 23:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL,
  `Assessor` varchar(45) DEFAULT NULL,
  `AssessDateTime` datetime DEFAULT NULL,
  `AssessPlace` varchar(45) DEFAULT NULL,
  `ServiceStartDate` datetime DEFAULT NULL,
  `ServicePause` int(2) DEFAULT NULL,
  `ServiceEndDate` datetime DEFAULT NULL,
  `ShiftPreference` varchar(10) DEFAULT NULL,
  `SpecificRequirements` varchar(45) DEFAULT NULL,
  `DaysWorked` varchar(45) DEFAULT NULL,
  `Productid` int(11) DEFAULT NULL,
  `Latitude` varchar(45) DEFAULT NULL,
  `Longitude` varchar(45) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=big5;



--
-- Table structure for table `circulatories`
--

CREATE TABLE `circulatories` (
  `id` int(11) NOT NULL,
  `ChestPain` varchar(45) DEFAULT NULL,
  `hoHTNCADCHF` varchar(45) DEFAULT NULL,
  `HR` varchar(45) DEFAULT NULL,
  `PeripheralCyanosis` varchar(5) DEFAULT NULL,
  `JugularVein` varchar(15) DEFAULT NULL,
  `SurgeryHistory` varchar(200) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bangalore', '2017-05-04 08:07:35', '2017-05-04 08:07:35'),
(2, 'Chennai', '2017-05-04 08:07:35', '2017-05-04 08:07:35'),
(3, 'Pune', '2017-05-04 08:07:35', '2017-05-04 08:07:35'),
(4, 'Hyderabad', '2017-05-04 08:07:35', '2017-05-04 08:07:35'),
(5, 'Hubbli-Dharwad', '2017-05-04 08:07:35', '2017-05-04 08:07:35');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(11) NOT NULL,
  `Language` varchar(45) DEFAULT NULL,
  `AdequateforAllActivities` varchar(45) DEFAULT NULL,
  `unableToCommunicate` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `dentures`
--

CREATE TABLE `dentures` (
  `id` int(11) NOT NULL,
  `Upper` varchar(15) DEFAULT NULL,
  `Lower` varchar(15) DEFAULT NULL,
  `Cleaning` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `FirstName` varchar(45) DEFAULT NULL,
  `LastName` varchar(45) DEFAULT NULL,
  `DOB` varchar(45) DEFAULT NULL,
  `Gender` varchar(45) DEFAULT NULL,
  `MartialStatus` varchar(45) DEFAULT NULL,
  `BloodGroup` varchar(45) DEFAULT NULL,
  `DOJ` varchar(45) DEFAULT NULL,
  `EmployementType` varchar(45) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `MobileNumber` varchar(45) DEFAULT NULL,
  `AlternateNumber` varchar(45) DEFAULT NULL,
  `Department` varchar(45) DEFAULT NULL,
  `under` int(5) DEFAULT NULL,
  `LangaugesKnown` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `FirstName`, `LastName`, `DOB`, `Gender`, `MartialStatus`, `BloodGroup`, `DOJ`, `EmployementType`, `Designation`, `MobileNumber`, `AlternateNumber`, `Department`, `under`, `LangaugesKnown`) VALUES
(3, 'Ligo', NULL, '1/1/1980', 'Male', 'Married', 'O+', '2/2/2017', 'Permanent', 'Vertical Head', '7894561230', NULL, 'PSC', NULL, 'English'),
(4, 'Sundara', NULL, '2/7/1980', 'Male', 'Single', 'A+', '3/2/2015', 'Permanent', 'Vertical Head', NULL, NULL, 'Physio', NULL, NULL),
(5, 'Sowmya', NULL, '5/7/1988', 'Female', 'Married', 'AB+', NULL, 'Permanent', 'Vertical Head', '794561230', NULL, 'Mathrutvam', NULL, 'All'),
(6, 'Sagar', NULL, NULL, 'Male', NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 4, NULL),
(7, 'Venkatesha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 4, NULL),
(8, 'Yathisha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 3, NULL),
(9, 'Shiju', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 3, NULL),
(10, 'Leelambika', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 5, NULL),
(11, 'Avinash', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Coordinator', NULL, NULL, NULL, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `enquiryproducts`
--

CREATE TABLE `enquiryproducts` (
  `id` int(11) NOT NULL,
  `Productid` int(11) NOT NULL,
  `leadid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enquiryservices`
--

CREATE TABLE `enquiryservices` (
  `id` int(11) NOT NULL,
  `ServiceId` int(11) NOT NULL,
  `leadid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `extremities`
--

CREATE TABLE `extremities` (
  `id` int(11) NOT NULL,
  `UppROM` varchar(45) DEFAULT NULL,
  `UppMuscleStrength` varchar(45) DEFAULT NULL,
  `LowROM` varchar(45) DEFAULT NULL,
  `LowMuscleStrength` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` int(11) NOT NULL,
  `gendertypes` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `gendertypes`, `created_at`, `updated_at`) VALUES
(1, 'Male', '2017-05-04 08:08:13', '2017-05-04 08:08:13'),
(2, 'Female', '2017-05-04 08:08:13', '2017-05-04 08:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `generalconditions`
--

CREATE TABLE `generalconditions` (
  `id` int(11) NOT NULL,
  `Weight` int(3) DEFAULT NULL,
  `Height` int(3) DEFAULT NULL,
  `Mobility` varchar(15) DEFAULT NULL,
  `Vision` varchar(45) DEFAULT NULL,
  `Hearing` varchar(45) DEFAULT NULL,
  `MedicalHistory` varchar(1000) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `generalhistories`
--

CREATE TABLE `generalhistories` (
  `id` int(11) NOT NULL,
  `PresentHistory` varchar(300) DEFAULT NULL,
  `PastHistory` varchar(300) DEFAULT NULL,
  `FamilyHistory` varchar(300) DEFAULT NULL,
  `Mensural_OBGHistory` varchar(300) DEFAULT NULL,
  `AllergicHistory` varchar(200) DEFAULT NULL,
  `AllergicStatus` varchar(15) DEFAULT NULL,
  `AllergicSeverity` varchar(15) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `genitos`
--

CREATE TABLE `genitos` (
  `id` int(11) NOT NULL,
  `UrinaryContinent` varchar(45) DEFAULT NULL,
  `HoUTIPOSTTURP` varchar(45) DEFAULT NULL,
  `CompletelyContinet` varchar(45) DEFAULT NULL,
  `IncontinentUrineOccasionally` varchar(45) DEFAULT NULL,
  `IncontinentUrineNightOnly` varchar(45) DEFAULT NULL,
  `IncontinentUrineAlways` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `immunizations`
--

CREATE TABLE `immunizations` (
  `id` int(11) NOT NULL,
  `DOB` timestamp NULL DEFAULT NULL,
  `Month` varchar(15) DEFAULT NULL,
  `VaccinationDate` timestamp NULL DEFAULT NULL,
  `VaccinationName` varchar(45) DEFAULT NULL,
  `DossageNumber` varchar(15) DEFAULT NULL,
  `IntervalBetweenDose` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `Languages` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `Languages`, `created_at`, `updated_at`) VALUES
(1, 'Hindi', '2017-05-03 11:32:18', '2017-05-03 11:32:18'),
(2, 'English', '2017-05-03 11:32:32', '2017-05-03 11:32:32'),
(3, 'Kannada', '2017-05-03 11:32:39', '2017-05-03 11:32:39'),
(4, 'Tamil', '2017-05-03 11:32:44', '2017-05-03 11:32:44'),
(5, 'Marathi', '2017-05-03 11:32:49', '2017-05-03 11:32:49'),
(6, 'Malayalam', '2017-05-03 11:32:54', '2017-05-03 11:32:54'),
(7, 'Telugu', '2017-05-03 11:32:59', '2017-05-04 04:00:17');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `fName` varchar(45) DEFAULT NULL,
  `mName` varchar(45) DEFAULT NULL,
  `lName` varchar(45) DEFAULT NULL,
  `EmailId` varchar(45) DEFAULT NULL,
  `Source` varchar(45) DEFAULT NULL,
  `MobileNumber` varchar(45) DEFAULT NULL,
  `Alternatenumber` varchar(45) DEFAULT NULL,
  `EmergencyContact` varchar(15) DEFAULT NULL,
  `AssesmentReq` varchar(4) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `Referenceid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `leadtypes`
--

CREATE TABLE `leadtypes` (
  `id` int(11) NOT NULL,
  `leadtypes` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leadtypes`
--

INSERT INTO `leadtypes` (`id`, `leadtypes`, `created_at`, `updated_at`) VALUES
(1, 'High Priority', '2017-05-04 08:08:50', '2017-05-10 00:32:47'),
(2, 'Medium Priority', '2017-05-04 08:08:50', '2017-05-10 00:33:01'),
(3, 'Low Priority', '2017-05-04 08:08:50', '2017-05-10 00:33:11');

-- --------------------------------------------------------

--
-- Table structure for table `mar`
--

CREATE TABLE `mar` (
  `id` int(11) NOT NULL,
  `Form` varchar(45) DEFAULT NULL,
  `Drug Name` varchar(1000) DEFAULT NULL,
  `Strength` varchar(15) DEFAULT NULL,
  `leadid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mathruthvams`
--

CREATE TABLE `mathruthvams` (
  `id` int(11) NOT NULL,
  `ChildDOB` varchar(45) DEFAULT NULL,
  `ChildMedicalDiagnosis` varchar(500) DEFAULT NULL,
  `ChildMedications` varchar(200) DEFAULT NULL,
  `ImmunizationUpdated` varchar(15) DEFAULT NULL,
  `BirthWeight` varchar(45) DEFAULT NULL,
  `DischargeWeight` varchar(45) DEFAULT NULL,
  `FeedingFormula` varchar(100) DEFAULT NULL,
  `FeedingIssue` varchar(100) DEFAULT NULL,
  `FeedingType` varchar(100) DEFAULT NULL,
  `FeedingEstablished` varchar(45) DEFAULT NULL,
  `FeedingMode` varchar(45) DEFAULT NULL,
  `MedicalConditions` varchar(45) DEFAULT NULL,
  `Length` varchar(45) DEFAULT NULL,
  `HeadCircumference` varchar(45) DEFAULT NULL,
  `SleepingPattern` varchar(15) DEFAULT NULL,
  `Motherid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `memoryintacts`
--

CREATE TABLE `memoryintacts` (
  `id` int(11) NOT NULL,
  `ShortTermIntact` varchar(45) DEFAULT NULL,
  `LongTermIntact` varchar(45) DEFAULT NULL,
  `ShortTermImpaired` varchar(45) DEFAULT NULL,
  `LongTermImpaired` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mobilities`
--

CREATE TABLE `mobilities` (
  `id` int(11) NOT NULL,
  `Independent` varchar(45) DEFAULT NULL,
  `NeedAssistance` varchar(45) DEFAULT NULL,
  `Walker` varchar(45) DEFAULT NULL,
  `WheelChair` varchar(45) DEFAULT NULL,
  `Crutch` varchar(45) DEFAULT NULL,
  `Cane` varchar(45) DEFAULT NULL,
  `ChairFast` varchar(45) DEFAULT NULL,
  `BedFast` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `mothers`
--

CREATE TABLE `mothers` (
  `id` int(11) NOT NULL,
  `FName` varchar(45) DEFAULT NULL,
  `MName` varchar(45) DEFAULT NULL,
  `LName` varchar(45) DEFAULT NULL,
  `Medications` varchar(500) DEFAULT NULL,
  `MedicalDiagnosis` varchar(200) DEFAULT NULL,
  `DeliveryPlace` varchar(45) DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `DeliveryType` varchar(15) DEFAULT NULL,
  `NoOfBabies` tinyint(4) DEFAULT NULL,
  `NoOfAttenderRequired` tinyint(4) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nutrition`
--

CREATE TABLE `nutrition` (
  `id` int(11) NOT NULL,
  `Adequate` varchar(45) DEFAULT NULL,
  `Diet` varchar(45) DEFAULT NULL,
  `BFTime` varchar(45) DEFAULT NULL,
  `LunchTime` varchar(45) DEFAULT NULL,
  `SnacksTime` varchar(45) DEFAULT NULL,
  `DinnerTime` varchar(45) DEFAULT NULL,
  `TPN` varchar(45) DEFAULT NULL,
  `RTFeeding` varchar(45) DEFAULT NULL,
  `PEGFeeding` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `orientations`
--

CREATE TABLE `orientations` (
  `id` int(11) NOT NULL,
  `Person` varchar(150) DEFAULT NULL,
  `Place` varchar(150) DEFAULT NULL,
  `Time` varchar(150) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personneldetails`
--

CREATE TABLE `personneldetails` (
  `id` int(11) NOT NULL,
  `PtfName` varchar(45) DEFAULT NULL,
  `PtmName` varchar(45) DEFAULT NULL,
  `PtlName` varchar(45) DEFAULT NULL,
  `age` varchar(10) DEFAULT NULL,
  `Gender` varchar(6) DEFAULT NULL,
  `Relationship` varchar(45) DEFAULT NULL,
  `Occupation` varchar(15) DEFAULT NULL,
  `AadharNum` varchar(45) DEFAULT NULL,
  `AlternateUHIDType` varchar(45) DEFAULT NULL,
  `AlternateUHIDNumber` varchar(45) DEFAULT NULL,
  `PTAwareofDisease` varchar(45) DEFAULT NULL,
  `Addressid` int(11) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `physioreports`
--

CREATE TABLE `physioreports` (
  `id` int(11) NOT NULL,
  `ProblemIdentified` varchar(100) DEFAULT NULL,
  `Treatment` varchar(200) DEFAULT NULL,
  `Physioid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `physiotheraphies`
--

CREATE TABLE `physiotheraphies` (
  `id` int(11) NOT NULL,
  `PhysiotheraphyType` varchar(45) DEFAULT NULL,
  `MetalImplant` varchar(45) DEFAULT NULL,
  `Hypertension` varchar(45) DEFAULT NULL,
  `Medications` varchar(45) DEFAULT NULL,
  `Osteoporosis` varchar(45) DEFAULT NULL,
  `Cancer` varchar(45) DEFAULT NULL,
  `PregnantOrBreastFeeding` varchar(45) DEFAULT NULL,
  `Diabetes` varchar(45) DEFAULT NULL,
  `ChronicInfection` varchar(45) DEFAULT NULL,
  `HeartDisease` varchar(45) DEFAULT NULL,
  `Epilepsy` varchar(45) DEFAULT NULL,
  `SurgeryUndergone` varchar(1000) DEFAULT NULL,
  `AffectedArea` varchar(500) DEFAULT NULL,
  `AssesmentDate` datetime DEFAULT NULL,
  `PainPattern` varchar(45) DEFAULT NULL,
  `ExaminationReport` varchar(300) DEFAULT NULL,
  `LabOrRadiologicalReport` varchar(300) DEFAULT NULL,
  `MedicalDisgnosis` varchar(500) DEFAULT NULL,
  `PhysiotherapeuticDiagnosis` varchar(100) DEFAULT NULL,
  `ShortTermGoal` varchar(100) DEFAULT NULL,
  `LongTermGoal` varchar(100) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `empid` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `SKUid` varchar(20) DEFAULT NULL,
  `ProductName` varchar(45) DEFAULT NULL,
  `DemoRequired` varchar(15) DEFAULT NULL,
  `AvailabilityStatus` varchar(45) DEFAULT NULL,
  `AvailabilityAddress` varchar(45) DEFAULT NULL,
  `SellingPrice` varchar(15) DEFAULT NULL,
  `RentalPrice` varchar(15) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `ptconditions`
--

CREATE TABLE `ptconditions` (
  `id` int(11) NOT NULL,
  `conditiontypes` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptconditions`
--

INSERT INTO `ptconditions` (`id`, `conditiontypes`, `created_at`, `updated_at`) VALUES
(1, 'Mobile', '2017-05-04 08:09:30', '2017-05-04 08:09:30'),
(2, 'Bedridden', '2017-05-04 08:09:30', '2017-05-04 08:09:30'),
(3, 'Parial', '2017-05-04 08:09:30', '2017-05-04 08:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE `references` (
  `id` int(11) NOT NULL,
  `Reference` varchar(45) DEFAULT NULL,
  `ReferalType` varchar(45) DEFAULT NULL,
  `ReferenceAddress` varchar(200) DEFAULT NULL,
  `ReferenceContact` varchar(15) DEFAULT NULL,
  `ReferenceEmail` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `Reference`, `ReferalType`, `ReferenceAddress`, `ReferenceContact`, `ReferenceEmail`, `created_at`, `updated_at`) VALUES
(1, 'Just Dial', 'Individual', NULL, NULL, NULL, '2017-05-03 12:08:02', '2017-05-03 12:08:02'),
(2, 'Apna Care', 'Individual', NULL, NULL, NULL, '2017-05-03 12:09:54', '2017-05-03 12:09:54'),
(3, 'BGS Hospital', 'Institute', NULL, NULL, NULL, '2017-05-03 12:10:21', '2017-05-03 12:10:21'),
(4, 'Good Hands', 'Institute', NULL, NULL, NULL, '2017-05-03 12:10:33', '2017-05-03 12:10:33'),
(5, 'Plus @ Home', 'Individual', NULL, NULL, NULL, '2017-05-03 12:10:48', '2017-05-03 12:10:48'),
(6, 'Bro4U', 'Individual', NULL, NULL, NULL, '2017-05-03 12:10:59', '2017-05-03 12:10:59'),
(7, 'Urban Clap', 'Individual', NULL, NULL, NULL, '2017-05-03 12:11:08', '2017-05-03 12:11:08'),
(8, 'People Tree Hospital', 'Institute', NULL, NULL, NULL, '2017-05-03 12:11:23', '2017-05-03 12:11:23'),
(9, 'Gayatri Hospital', 'Institute', NULL, NULL, NULL, '2017-05-03 12:11:33', '2017-05-03 12:11:33'),
(10, 'Rangadore Hospital', 'Institute', NULL, NULL, NULL, '2017-05-03 12:11:46', '2017-05-03 12:11:46'),
(11, 'Karunashraya', 'Institute', NULL, NULL, NULL, '2017-05-03 12:12:00', '2017-05-03 12:12:00'),
(12, 'Super Receptionist', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:09', '2017-05-03 12:12:09'),
(13, 'Homecues HYD', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:19', '2017-05-03 12:12:19'),
(14, 'Existing Client', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:28', '2017-05-03 12:12:28'),
(15, 'Pure Chat', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:36', '2017-05-03 12:12:36'),
(16, 'Vasavi Hospital', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:44', '2017-05-03 12:12:44'),
(17, 'Nationwide Doctors', 'Individual', NULL, NULL, NULL, '2017-05-03 12:12:52', '2017-05-03 12:12:52'),
(18, 'Global Hospitals Chennai', 'Institute', NULL, NULL, NULL, '2017-05-03 12:13:02', '2017-05-03 12:13:02'),
(19, 'Life in Hospital Bangalore', 'Institute', NULL, NULL, NULL, '2017-05-03 12:13:13', '2017-05-03 12:13:13'),
(20, 'Saral Health', 'Institute', NULL, NULL, NULL, '2017-05-03 12:13:26', '2017-05-03 12:13:26'),
(21, 'People tree', 'Hospital', NULL, NULL, NULL, '2017-05-25 02:10:56', '2017-05-25 02:10:56');

-- --------------------------------------------------------

--
-- Table structure for table `relationships`
--

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL,
  `relationshiptype` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relationships`
--

INSERT INTO `relationships` (`id`, `relationshiptype`, `created_at`, `updated_at`) VALUES
(1, 'Self', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(2, 'Spouse', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(3, 'Father', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(4, 'Mother', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(5, 'Son', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(6, 'Daughter', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(7, 'Brother', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(8, 'Sister', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(9, 'Father in law', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(10, 'Mother in law', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(11, 'Daughter in law', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(12, 'Son in law', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(13, 'Grand Father', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(14, 'Grand Mother', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(15, 'Grandson', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(16, 'Grand Daughter', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(17, 'Uncle', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(18, 'Aunt', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(19, 'Friend', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(20, 'Cousin', '2017-05-04 08:10:05', '2017-05-04 08:10:05'),
(21, 'Other', '2017-05-04 08:10:05', '2017-05-04 08:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `respiratories`
--

CREATE TABLE `respiratories` (
  `id` int(11) NOT NULL,
  `SOB` varchar(45) DEFAULT NULL,
  `Cough` varchar(45) DEFAULT NULL,
  `ColorOfPhlegm` varchar(45) DEFAULT NULL,
  `Nebulization` varchar(45) DEFAULT NULL,
  `Tracheostomy` varchar(45) DEFAULT NULL,
  `CPAP_BIPAP` varchar(45) DEFAULT NULL,
  `ICD` varchar(45) DEFAULT NULL,
  `Position` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `H_OTB_Asthma_COPD` varchar(45) DEFAULT NULL,
  `SPO2` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'customercare', '2017-05-26 06:41:35', '2017-05-26 06:41:35'),
(2, 'vertical', '2017-05-26 06:41:35', '2017-05-26 06:41:35'),
(3, 'admin', '2017-05-26 07:17:15', '2017-05-26 07:17:15'),
(4, 'super_user', '2017-05-31 06:23:54', '2017-05-31 06:23:54'),
(5, 'Management', '2017-05-31 06:24:19', '2017-05-31 06:24:19'),
(6, 'Coordinator', '2017-05-31 06:24:54', '2017-05-31 06:24:54'),
(7, 'Care_provider', '2017-05-31 06:25:16', '2017-05-31 06:25:16');

-- --------------------------------------------------------

--
-- Table structure for table `role_admins`
--

CREATE TABLE `role_admins` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_admins`
--

INSERT INTO `role_admins` (`id`, `role_id`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2017-05-26 06:51:21', '2017-05-26 06:51:21'),
(2, 2, 3, '2017-05-26 06:51:21', '2017-05-26 06:51:21'),
(3, 3, 1, '2017-05-26 07:17:22', '2017-05-26 07:17:22'),
(4, 5, 6, '2017-05-31 06:35:48', '2017-05-31 06:35:48'),
(5, 5, 7, '2017-05-31 06:35:48', '2017-05-31 06:35:48'),
(6, 5, 8, '2017-05-31 06:36:00', '2017-05-31 06:36:00'),
(7, 3, 9, '2017-05-31 06:36:34', '2017-05-31 06:36:34'),
(8, 3, 10, '2017-05-31 06:36:34', '2017-05-31 06:36:34'),
(9, 2, 11, '2017-05-31 06:36:56', '2017-05-31 06:36:56'),
(10, 2, 12, '2017-05-31 06:36:56', '2017-05-31 06:36:56'),
(11, 6, 13, '2017-05-31 08:28:04', '2017-05-31 08:28:04'),
(12, 6, 14, '2017-05-31 08:28:04', '2017-05-31 08:28:04'),
(13, 6, 15, '2017-05-31 08:28:14', '2017-05-31 08:28:14'),
(14, 6, 16, '2017-05-31 08:28:14', '2017-05-31 08:28:14'),
(15, 2, 17, '2017-06-01 11:10:55', '2017-06-01 11:10:55'),
(16, 1, 19, '2017-06-01 11:58:56', '2017-06-01 11:58:56'),
(17, 6, 20, '2017-06-02 11:14:43', '2017-06-02 11:14:43'),
(18, 6, 21, '2017-06-02 11:14:43', '2017-06-02 11:14:43');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `ServiceType` varchar(25) DEFAULT NULL,
  `GeneralCondition` varchar(15) DEFAULT NULL,
  `LeadType` varchar(15) DEFAULT NULL,
  `Branch` varchar(45) DEFAULT NULL,
  `RequestDateTime` datetime DEFAULT NULL,
  `AssignedTo` varchar(45) DEFAULT NULL,
  `QuotedPrice` int(11) DEFAULT NULL,
  `ExpectedPrice` int(11) DEFAULT NULL,
  `ServiceStatus` varchar(15) DEFAULT NULL,
  `PreferedGender` varchar(25) DEFAULT NULL,
  `PreferedLanguage` varchar(15) DEFAULT NULL,
  `Remarks` varchar(500) DEFAULT NULL,
  `langId` int(11) DEFAULT NULL,
  `Leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `shiftrequireds`
--

CREATE TABLE `shiftrequireds` (
  `id` int(11) NOT NULL,
  `shiftrequired` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shiftrequireds`
--

INSERT INTO `shiftrequireds` (`id`, `shiftrequired`, `created_at`, `updated_at`) VALUES
(1, 'Day', '2017-05-04 08:11:16', '2017-05-29 04:52:30'),
(2, 'Night', '2017-05-04 08:11:16', '2017-05-29 04:52:38'),
(3, 'Live in Care', '2017-05-04 08:11:16', '2017-05-29 04:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `admin`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'murtaza', 1, 'murtazasalumber@ymail.com', '$2y$10$uc.c5pVBtF0vYIzmBSIn0en6KDWAbc.9Fkf8j5xSHIPOtgFx9yv2q', '00lczB4gNVewOlXziAmCpRzovPDRi2Rv0eu8UWAE71mce52qpWfM2EGS9nlS', '2017-05-08 23:53:05', '2017-05-24 04:24:13'),
(2, 'murtaza', 0, 'murtazasalumber007@gmail.com', '$2y$10$V9RrCzW4J4CJEsi1Ec5NEeLW731Defe57xE3uTMmVakL3MdEZG5qy', 'oxUhFwcAjKMYLwborwXcDFIXrmzKRPn7xaFcQnLEDIoF9XAHLWZIDvB16jKN', '2017-05-09 00:15:28', '2017-05-24 02:23:43'),
(3, 'arun', 0, 'arun@gmail.com', '$2y$10$TeTQA0YlxyUcY5HRtsUgNeJasMYibcaCNpJ.WOVaHXMqKEH0QMBs6', 'gxOvj7BcCy5RzqgGTom1jq3rjEqtPd1drBDm4ZkobsNKiyx1sjAFIUkvv3SW', '2017-05-24 00:39:54', '2017-05-24 00:39:54'),
(4, 'abbas', 0, 'abbas123@gmail.com', '$2y$10$hUS3UWJqpYyWZWAm4rx6Seg0aPinEW5G/R9um9xOE4NO9yVTz.CTy', '40Sgxtujj60Kqcc7BzJ0G6zneH36KpbwkVNAP7lKIreC4JAkvGC36qrMhmjN', '2017-05-30 06:47:41', '2017-05-30 06:47:41'),
(5, 'abbas', 0, 'abbas@gmail.com', '$2y$10$xkFg2JjyobNl9fwFt7VtouPLouCnZkBKr1SWJQ.0A/33a43TGc3Ki', NULL, '2017-05-30 06:52:03', '2017-05-30 06:52:03');

-- --------------------------------------------------------

--
-- Table structure for table `verticalcoordinations`
--

CREATE TABLE `verticalcoordinations` (
  `id` int(11) NOT NULL,
  `leadid` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



--
-- Table structure for table `verticalrefs`
--

CREATE TABLE `verticalrefs` (
  `id` int(11) NOT NULL,
  `AbdomenId` int(11) DEFAULT NULL,
  `CirculatoryId` int(11) DEFAULT NULL,
  `CommunicationId` int(11) DEFAULT NULL,
  `DentureId` int(11) DEFAULT NULL,
  `ExtremitiesId` int(11) DEFAULT NULL,
  `GenitoId` int(11) DEFAULT NULL,
  `generalconditionId` int(11) DEFAULT NULL,
  `generalhistoryId` int(11) DEFAULT NULL,
  `MemoryIntactId` int(11) DEFAULT NULL,
  `MobilityId` int(11) DEFAULT NULL,
  `NutitionId` int(11) DEFAULT NULL,
  `VitalsignId` int(11) DEFAULT NULL,
  `RespiratoryId` int(11) DEFAULT NULL,
  `VisionHearingId` int(11) DEFAULT NULL,
  `OrientationId` int(11) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `verticals`
--

CREATE TABLE `verticals` (
  `id` int(11) NOT NULL,
  `verticaltype` varchar(45) NOT NULL,
  `servicetype` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verticals`
--

INSERT INTO `verticals` (`id`, `verticaltype`, `servicetype`, `created_at`, `updated_at`) VALUES
(5, 'Registered Nurses', 'Respiratory Care', '2017-05-04 08:10:38', '2017-05-08 07:12:21'),
(6, 'Registered Nurses', 'Intravenous Therapy', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(7, 'Registered Nurses', 'Medication Administration', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(8, 'Registered Nurses', 'Nutrition', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(9, 'Registered Nurses', 'Assisting in Elimination', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(10, 'Registered Nurses', 'Post Operative Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(11, 'Registered Nurses', 'Diabetic/Wound Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(12, 'Registered Nurses', 'Rehabilitation', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(13, 'Personal Supportive Care', 'Personal Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(14, 'Personal Supportive Care', 'Companionship', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(15, 'Personal Supportive Care', 'Live -in Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(16, 'Personal Supportive Care', 'Palliative/End of Life Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(17, 'Personal Supportive Care', 'Alzheimers and Dementia', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(18, 'Physiotherapy - Home', 'Physiotherapy', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(19, 'Mathrutvam - Baby Care', 'Infant Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(20, 'Mathrutvam - Baby Care', 'Neonatal', '2017-05-04 08:10:38', '2017-05-04 08:10:38'),
(21, 'Mathrutvam - Baby Care', 'Nanny Care', '2017-05-04 08:10:38', '2017-05-04 08:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `visionhearings`
--

CREATE TABLE `visionhearings` (
  `id` int(11) NOT NULL,
  `Impared` varchar(5) DEFAULT NULL,
  `ShortSight` varchar(5) DEFAULT NULL,
  `LongSight` varchar(5) DEFAULT NULL,
  `WearsGlasses` varchar(5) DEFAULT NULL,
  `HearingAids` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `vitalsigns`
--

CREATE TABLE `vitalsigns` (
  `id` int(11) NOT NULL,
  `BP` varchar(15) DEFAULT NULL,
  `RR` varchar(15) DEFAULT NULL,
  `Temperature` varchar(15) DEFAULT NULL,
  `TemperatureType` varchar(45) DEFAULT NULL,
  `P1` varchar(200) DEFAULT NULL,
  `P2` varchar(200) DEFAULT NULL,
  `P3` varchar(200) DEFAULT NULL,
  `R1` varchar(200) DEFAULT NULL,
  `R2` varchar(200) DEFAULT NULL,
  `R3` varchar(200) DEFAULT NULL,
  `R4` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `Pulse` varchar(15) DEFAULT NULL,
  `PainScale` varchar(45) DEFAULT NULL,
  `Quality` varchar(45) DEFAULT NULL,
  `SeverityScale` varchar(45) DEFAULT NULL,
  `leadid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Indexes for dumped tables
--

--
-- Indexes for table `abdomens`
--
ALTER TABLE `abdomens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Abdomens_1_idx` (`leadid`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Address_1_idx` (`leadid`),
  ADD KEY `empid` (`empid`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `assessments`
--
ALTER TABLE `assessments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Assessment_3_idx` (`Leadid`),
  ADD KEY `fk_Assessment_2_idx` (`Productid`);

--
-- Indexes for table `circulatories`
--
ALTER TABLE `circulatories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Circulatorys_1_idx` (`leadid`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Communications_1_idx` (`leadid`);

--
-- Indexes for table `dentures`
--
ALTER TABLE `dentures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Dentures_1_idx` (`leadid`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiryproducts`
--
ALTER TABLE `enquiryproducts`
  ADD PRIMARY KEY (`id`,`Productid`,`leadid`),
  ADD KEY `fk_EnquiryProduct_1_idx` (`leadid`),
  ADD KEY `fk_EnquiryProduct_2_idx` (`Productid`);

--
-- Indexes for table `enquiryservices`
--
ALTER TABLE `enquiryservices`
  ADD PRIMARY KEY (`id`,`ServiceId`,`leadid`),
  ADD KEY `fk_EnquiryServices_1_idx` (`ServiceId`),
  ADD KEY `fk_EnquiryServices_2_idx` (`leadid`);

--
-- Indexes for table `extremities`
--
ALTER TABLE `extremities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Extremities_1_idx` (`leadid`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generalconditions`
--
ALTER TABLE `generalconditions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_MedicalCondition_1_idx` (`Leadid`);

--
-- Indexes for table `generalhistories`
--
ALTER TABLE `generalhistories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_MedicalDiagnosis_1_idx` (`leadid`);

--
-- Indexes for table `genitos`
--
ALTER TABLE `genitos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Genitos_1_idx` (`leadid`);

--
-- Indexes for table `immunizations`
--
ALTER TABLE `immunizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leadid` (`leadid`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Lead_2_idx` (`Referenceid`),
  ADD KEY `empid` (`empid`);

--
-- Indexes for table `leadtypes`
--
ALTER TABLE `leadtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mar`
--
ALTER TABLE `mar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leadid` (`leadid`);

--
-- Indexes for table `mathruthvams`
--
ALTER TABLE `mathruthvams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Mathruthvams_1_idx` (`Motherid`);

--
-- Indexes for table `memoryintacts`
--
ALTER TABLE `memoryintacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_MemoryIntacts_1_idx` (`leadid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobilities`
--
ALTER TABLE `mobilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Mobilitys_1_idx` (`leadid`);

--
-- Indexes for table `mothers`
--
ALTER TABLE `mothers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Mother_1_idx` (`Leadid`);

--
-- Indexes for table `nutrition`
--
ALTER TABLE `nutrition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Nutritions_1_idx` (`leadid`);

--
-- Indexes for table `orientations`
--
ALTER TABLE `orientations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Orientations_1_idx` (`leadid`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personneldetails`
--
ALTER TABLE `personneldetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_PersonnelDetails_1_idx` (`Leadid`),
  ADD KEY `Addressid` (`Addressid`);

--
-- Indexes for table `physioreports`
--
ALTER TABLE `physioreports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_PhysioReport_1_idx` (`Physioid`);

--
-- Indexes for table `physiotheraphies`
--
ALTER TABLE `physiotheraphies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Physiotheraphy_1_idx` (`Leadid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Product_1_idx` (`Leadid`);

--
-- Indexes for table `ptconditions`
--
ALTER TABLE `ptconditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `references`
--
ALTER TABLE `references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `respiratories`
--
ALTER TABLE `respiratories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Respiratorys_1_idx` (`leadid`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_admins`
--
ALTER TABLE `role_admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Services_1_idx` (`langId`),
  ADD KEY `fk_Services_2_idx` (`Leadid`);

--
-- Indexes for table `shiftrequireds`
--
ALTER TABLE `shiftrequireds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `verticalcoordinations`
--
ALTER TABLE `verticalcoordinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empid` (`empid`),
  ADD KEY `leadid` (`leadid`);

--
-- Indexes for table `verticalrefs`
--
ALTER TABLE `verticalrefs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_VerticalRef_2_idx` (`CirculatoryId`),
  ADD KEY `fk_VerticalRef_3_idx` (`CommunicationId`),
  ADD KEY `fk_VerticalRef_4_idx` (`DentureId`),
  ADD KEY `fk_VerticalRef_5_idx` (`ExtremitiesId`),
  ADD KEY `fk_VerticalRef_6_idx` (`GenitoId`),
  ADD KEY `fk_VerticalRef_7_idx` (`generalconditionId`),
  ADD KEY `fk_VerticalRef_8_idx` (`MemoryIntactId`),
  ADD KEY `fk_VerticalRef_9_idx` (`RespiratoryId`),
  ADD KEY `fk_VerticalRef_10_idx` (`MobilityId`),
  ADD KEY `fk_VerticalRef_11_idx` (`VisionHearingId`),
  ADD KEY `fk_VerticalRef_12_idx` (`NutitionId`),
  ADD KEY `fk_VerticalRef_13_idx` (`VitalsignId`),
  ADD KEY `fk_VerticalRef_14_idx` (`leadid`),
  ADD KEY `fk_VerticalRef_1_idx` (`AbdomenId`),
  ADD KEY `fk_VerticalRefs_1_idx` (`OrientationId`);

--
-- Indexes for table `verticals`
--
ALTER TABLE `verticals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visionhearings`
--
ALTER TABLE `visionhearings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_VisionHearings_1_idx` (`leadid`);

--
-- Indexes for table `vitalsigns`
--
ALTER TABLE `vitalsigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Pains_1_idx` (`leadid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abdomens`
--
ALTER TABLE `abdomens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `assessments`
--
ALTER TABLE `assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `circulatories`
--
ALTER TABLE `circulatories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `dentures`
--
ALTER TABLE `dentures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `enquiryproducts`
--
ALTER TABLE `enquiryproducts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enquiryservices`
--
ALTER TABLE `enquiryservices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `extremities`
--
ALTER TABLE `extremities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `generalconditions`
--
ALTER TABLE `generalconditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `generalhistories`
--
ALTER TABLE `generalhistories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `genitos`
--
ALTER TABLE `genitos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `immunizations`
--
ALTER TABLE `immunizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `leadtypes`
--
ALTER TABLE `leadtypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mar`
--
ALTER TABLE `mar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mathruthvams`
--
ALTER TABLE `mathruthvams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `memoryintacts`
--
ALTER TABLE `memoryintacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mobilities`
--
ALTER TABLE `mobilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `mothers`
--
ALTER TABLE `mothers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nutrition`
--
ALTER TABLE `nutrition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `orientations`
--
ALTER TABLE `orientations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `personneldetails`
--
ALTER TABLE `personneldetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `physioreports`
--
ALTER TABLE `physioreports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `physiotheraphies`
--
ALTER TABLE `physiotheraphies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ptconditions`
--
ALTER TABLE `ptconditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `references`
--
ALTER TABLE `references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `relationships`
--
ALTER TABLE `relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `respiratories`
--
ALTER TABLE `respiratories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `role_admins`
--
ALTER TABLE `role_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `shiftrequireds`
--
ALTER TABLE `shiftrequireds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `verticalcoordinations`
--
ALTER TABLE `verticalcoordinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `verticalrefs`
--
ALTER TABLE `verticalrefs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `verticals`
--
ALTER TABLE `verticals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `visionhearings`
--
ALTER TABLE `visionhearings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `vitalsigns`
--
ALTER TABLE `vitalsigns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `abdomens`
--
ALTER TABLE `abdomens`
  ADD CONSTRAINT `fk_Abdomens_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`empid`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `fk_Address_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `assessments`
--
ALTER TABLE `assessments`
  ADD CONSTRAINT `fk_Assessment_2` FOREIGN KEY (`Productid`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Assessment_3` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `circulatories`
--
ALTER TABLE `circulatories`
  ADD CONSTRAINT `fk_Circulatorys_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `communications`
--
ALTER TABLE `communications`
  ADD CONSTRAINT `fk_Communications_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dentures`
--
ALTER TABLE `dentures`
  ADD CONSTRAINT `fk_Dentures_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `enquiryproducts`
--
ALTER TABLE `enquiryproducts`
  ADD CONSTRAINT `fk_EnquiryProduct_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_EnquiryProduct_2` FOREIGN KEY (`Productid`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `enquiryservices`
--
ALTER TABLE `enquiryservices`
  ADD CONSTRAINT `fk_EnquiryServices_1` FOREIGN KEY (`ServiceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_EnquiryServices_2` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `extremities`
--
ALTER TABLE `extremities`
  ADD CONSTRAINT `fk_Extremities_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `generalconditions`
--
ALTER TABLE `generalconditions`
  ADD CONSTRAINT `fk_MedicalCondition_1` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `generalhistories`
--
ALTER TABLE `generalhistories`
  ADD CONSTRAINT `fk_MedicalDiagnosis_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `genitos`
--
ALTER TABLE `genitos`
  ADD CONSTRAINT `fk_Genitos_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `fk_Lead_2` FOREIGN KEY (`Referenceid`) REFERENCES `references` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`empid`) REFERENCES `employees` (`id`);

--
-- Constraints for table `mathruthvams`
--
ALTER TABLE `mathruthvams`
  ADD CONSTRAINT `fk_Mathruthvams_1` FOREIGN KEY (`Motherid`) REFERENCES `mothers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `memoryintacts`
--
ALTER TABLE `memoryintacts`
  ADD CONSTRAINT `fk_MemoryIntacts_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mobilities`
--
ALTER TABLE `mobilities`
  ADD CONSTRAINT `fk_Mobilitys_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mothers`
--
ALTER TABLE `mothers`
  ADD CONSTRAINT `fk_Mother_1` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `nutrition`
--
ALTER TABLE `nutrition`
  ADD CONSTRAINT `fk_Nutritions_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orientations`
--
ALTER TABLE `orientations`
  ADD CONSTRAINT `fk_Orientations_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `personneldetails`
--
ALTER TABLE `personneldetails`
  ADD CONSTRAINT `fk_PersonnelDetails_1` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personneldetails_ibfk_1` FOREIGN KEY (`Addressid`) REFERENCES `addresses` (`id`);

--
-- Constraints for table `physioreports`
--
ALTER TABLE `physioreports`
  ADD CONSTRAINT `fk_PhysioReport_1` FOREIGN KEY (`Physioid`) REFERENCES `physiotheraphies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `physiotheraphies`
--
ALTER TABLE `physiotheraphies`
  ADD CONSTRAINT `fk_Physiotheraphy_1` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_Product_1` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `respiratories`
--
ALTER TABLE `respiratories`
  ADD CONSTRAINT `fk_Respiratorys_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `fk_Services_1` FOREIGN KEY (`langId`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Services_2` FOREIGN KEY (`Leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `verticalrefs`
--
ALTER TABLE `verticalrefs`
  ADD CONSTRAINT `fk_VerticalRef_1` FOREIGN KEY (`AbdomenId`) REFERENCES `abdomens` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_VerticalRef_10` FOREIGN KEY (`MobilityId`) REFERENCES `mobilities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_11` FOREIGN KEY (`VisionHearingId`) REFERENCES `visionhearings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_12` FOREIGN KEY (`NutitionId`) REFERENCES `nutrition` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_13` FOREIGN KEY (`VitalsignId`) REFERENCES `vitalsigns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_14` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_2` FOREIGN KEY (`CirculatoryId`) REFERENCES `circulatories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_3` FOREIGN KEY (`CommunicationId`) REFERENCES `communications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_4` FOREIGN KEY (`DentureId`) REFERENCES `dentures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_5` FOREIGN KEY (`ExtremitiesId`) REFERENCES `extremities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_6` FOREIGN KEY (`GenitoId`) REFERENCES `genitos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_7` FOREIGN KEY (`generalconditionId`) REFERENCES `generalconditions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_8` FOREIGN KEY (`MemoryIntactId`) REFERENCES `memoryintacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRef_9` FOREIGN KEY (`RespiratoryId`) REFERENCES `respiratories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VerticalRefs_1` FOREIGN KEY (`OrientationId`) REFERENCES `orientations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `visionhearings`
--
ALTER TABLE `visionhearings`
  ADD CONSTRAINT `fk_VisionHearings_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vitalsigns`
--
ALTER TABLE `vitalsigns`
  ADD CONSTRAINT `fk_Pains_1` FOREIGN KEY (`leadid`) REFERENCES `leads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
